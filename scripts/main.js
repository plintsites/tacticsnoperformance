// Good ol' ES5 require statements to import React and Underscore
var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('underscore');

// ES6 import of our own helper functions
import h from './helpers';

/*
  Tactics (main app)
  This will let us make <Tactics/>
*/

var Tactics = React.createClass({

	/* No PropTypes because this is the main app which will be based on the State */

	getInitialState: function() {

		return {
			startTurn: 0,
			gameTurn: 1,
			gameFinished: false,
			player1: {
				name: 'Player 1',
				roundsWon: 0,
				turn: true,
				score: {
					'20': {hits: 0, completed: false},
					'19': {hits: 0, completed: false},
					'18': {hits: 0, completed: false},
					'17': {hits: 0, completed: false},
					'16': {hits: 0, completed: false},
					'15': {hits: 0, completed: false},
					'14': {hits: 0, completed: false},
					'13': {hits: 0, completed: false},
					'12': {hits: 0, completed: false},
					'11': {hits: 0, completed: false},
					'10': {hits: 0, completed: false}
				}
			},
			player2: {
				name: 'Player 2',
				roundsWon: 0,
				turn: false,
				score: {
					'20': {hits: 0, completed: false},
					'19': {hits: 0, completed: false},
					'18': {hits: 0, completed: false},
					'17': {hits: 0, completed: false},
					'16': {hits: 0, completed: false},
					'15': {hits: 0, completed: false},
					'14': {hits: 0, completed: false},
					'13': {hits: 0, completed: false},
					'12': {hits: 0, completed: false},
					'11': {hits: 0, completed: false},
					'10': {hits: 0, completed: false}
				}
			}
		};
	},

	changeTurn: function() {
		// update the state object
		this.state.player1.turn = !this.state.player1.turn;
		this.state.player2.turn = !this.state.player2.turn;
		this.state.gameTurn     = 3 - this.state.gameTurn;
	    // set the state to rerender
	    this.setState({
	    	player1: this.state.player1,
	    	player2: this.state.player2,
	    	gameTurn: this.state.gameTurn
	    });
	},

	hitNumber: function(number, active) {
		// check if clicked on active board
		if (active) {

			// get player state and opponent state
			var playerKey = 'player' + this.state.gameTurn;
			var otherPlayerKey = 'player' + (3 - this.state.gameTurn);
			var player = this.state[playerKey];
			var otherPlayer = this.state[otherPlayerKey];

			if (player.score[number].hits < 3) {
				// update number of hits for this player
				player.score[number].hits += 1;

				// create empty object for updating state
				var update = {};
				// add current player state to update object
				update[playerKey] = player;

				// mark as complete if it is completed now, note that this is part of state for the other player
				if (player.score[number].hits === 3) {
					// update opponent completed status for this number
					otherPlayer.score[number].completed = true;
					// add opponent player state to update object
					update[otherPlayerKey] = otherPlayer;
				}

				// Check if game is finished and if so, update state for this also
				if (this.gameFinished()) {
					update.gameFinished = true;

					// update standings!
					var points1 = h.calculateScore(player.score);
					var points2 = h.calculateScore(otherPlayer.score);

					if (points1 > points2) {
						player.roundsWon += 1;
						update[playerKey] = player;
					} else if (points2 > points1) {
						otherPlayer.roundsWon += 1;
						update[otherPlayerKey] = otherPlayer;
					}
				}

				// set the state to rerender
				this.setState(update);
			}
			// else {
			// 	console.log('You completed this number already');
			// }

		}
		// else {
		// 	console.log('board not active: it is not your turn');
		// }
	},

	addPoints: function(number) {
		// get player state
		var playerKey = 'player' + this.state.gameTurn;

		// update number of hits for number
		this.state[playerKey].score[number].hits += 1;

		// setup update object to set state afterwards
		var update = {};
		update[playerKey] = this.state[playerKey];
		this.setState(update);
	},

	// check if a game is finished (called from hitNumber)
	gameFinished: function() {
		var player1Finished = true;
		var player2Finished = true;
		for (var i = 10; i <= 20; i++) {
			player1Finished = player1Finished && this.state.player1.score[i].completed;
			player2Finished = player2Finished && this.state.player2.score[i].completed;
		}

		return player1Finished && player2Finished;
	},

	// reset state for a new game
	resetGame: function() {
		this.state.startTurn = !this.state.startTurn;
		this.state.gameTurn = 1 + this.state.startTurn;
		this.state.gameFinished = false;
		this.state.player1.turn = !this.state.startTurn;
		this.state.player2.turn = this.state.startTurn;
		this.state.player1.score = {
			'20': {hits: 0, completed: false},
			'19': {hits: 0, completed: false},
			'18': {hits: 0, completed: false},
			'17': {hits: 0, completed: false},
			'16': {hits: 0, completed: false},
			'15': {hits: 0, completed: false},
			'14': {hits: 0, completed: false},
			'13': {hits: 0, completed: false},
			'12': {hits: 0, completed: false},
			'11': {hits: 0, completed: false},
			'10': {hits: 0, completed: false}
		};
		this.state.player2.score = {
			'20': {hits: 0, completed: false},
			'19': {hits: 0, completed: false},
			'18': {hits: 0, completed: false},
			'17': {hits: 0, completed: false},
			'16': {hits: 0, completed: false},
			'15': {hits: 0, completed: false},
			'14': {hits: 0, completed: false},
			'13': {hits: 0, completed: false},
			'12': {hits: 0, completed: false},
			'11': {hits: 0, completed: false},
			'10': {hits: 0, completed: false}
		};
		this.setState(this.state);
	},

	render: function() {
		// Figure out if game is finished
		var finished = '';
		if (this.state.gameFinished) {
			finished = <Finished player1={this.state.player1} player2={this.state.player2} resetGame={this.resetGame}/>;
		}
		return (
			<div className="tactics">
				<Header tagline="Tactics"/>
				<TacticsGame player1={this.state.player1} player2={this.state.player2} changeTurn={this.changeTurn} hitNumber={this.hitNumber} addPoints={this.addPoints}/>
				{finished}
			</div>
		)
	}
});

/*
	Header
	<Header />
 */

var Header = React.createClass({

	propTypes: {
		tagline: React.PropTypes.string.isRequired
	},

	render: function() {
		return (
			<header>
				<nav className="navbar navbar-default">
					<div className="container-fluid">
						<div className="navbar-header">
							<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
								<span className="sr-only">Toggle navigation</span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
							</button>
							<a className="navbar-brand" href="#">{this.props.tagline}</a>
						</div>
						<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
							<ul className="nav navbar-nav">
								<li className="active"><a href="#">Home (main menu)</a></li>
								<li><a href="#">Settings</a></li>
								<li><a href="#">Help</a></li>
							</ul>
						</div>
					</div>
				</nav>
			</header>
		)
	}
});

/*
	TacticsGame component: is where the actual game will live
	<TacticsGame />
 */

var TacticsGame = React.createClass({

	propTypes: {
		player1: React.PropTypes.object.isRequired,
		player2: React.PropTypes.object.isRequired,
		changeTurn: React.PropTypes.func.isRequired,
		hitNumber: React.PropTypes.func.isRequired,
		addPoints: React.PropTypes.func.isRequired
	},

	render: function() {
		var points = 56;
		return (
			<div className="tactics-container">
				<div className="gamefields">
					<PlayerField player={this.props.player1} hitNumber={this.props.hitNumber} addPoints={this.props.addPoints}/>
					<PlayerTurner changeTurn={this.props.changeTurn}/>
					<PlayerField player={this.props.player2} hitNumber={this.props.hitNumber} addPoints={this.props.addPoints}/>
				</div>
				<Standings wonPlayer1={this.props.player1.roundsWon} wonPlayer2={this.props.player2.roundsWon}/>
			</div>
		)
	}
});

/*
	PlayerField component: interface for a single player
	<PlayerField />
 */

var PlayerField = React.createClass({

	propTypes: {
		player: React.PropTypes.object.isRequired,
		hitNumber: React.PropTypes.func.isRequired,
		addPoints: React.PropTypes.func.isRequired
	},

	renderRow: function(item) {
		var score = this.props.player.score;
		return <Row key={item} number={item} hits={score[item].hits} completedByOpponent={score[item].completed} turn={this.props.player.turn} hitNumber={this.props.hitNumber} addPoints={this.props.addPoints}/>
	},

	render: function() {
		var gameClass = 'gamefield';
		if (this.props.player.turn) {
			gameClass += ' turn';
		}
		/* Compute the points sofar for this player */
		var points = h.calculateScore(this.props.player.score);

		return (
			<div className={gameClass}>
				<h2>{this.props.player.name} (<span className="player-points">{points}</span>)</h2>
				{_.range(20,9,-1).map(this.renderRow)}
			</div>
		)
	}
});

/*
	Row component: to display a row with players progress in the game for a single number
	<Row />
 */

var Row = React.createClass({

	propTypes: {
		number: React.PropTypes.number.isRequired,
		hitNumber: React.PropTypes.func.isRequired,
		hits: React.PropTypes.number.isRequired,
		completedByOpponent: React.PropTypes.bool.isRequired,
		addPoints: React.PropTypes.func.isRequired,
		turn: React.PropTypes.bool.isRequired
	},

	render: function() {
		var cntrClass = 'tactics-row';
		var status = 'hidden';
		if (this.props.hits >= 3) {
			cntrClass += ' row-completed';
			status = '';
		}
		var itemClass = 'row-item hit-' + Math.min(3, this.props.hits);

		return (
			<div className={cntrClass}>
				<span className={itemClass} data-item="1" onClick={this.props.hitNumber.bind(null, this.props.number, this.props.turn)}>{this.props.number}</span>
				<span className={itemClass} data-item="2" onClick={this.props.hitNumber.bind(null, this.props.number, this.props.turn)}>{this.props.number}</span>
				<span className={itemClass} data-item="3" onClick={this.props.hitNumber.bind(null, this.props.number, this.props.turn)}>{this.props.number}</span>
				<PointsCounter amount={Math.max(0, this.props.number*(this.props.hits - 3))} status={status} completedByOpponent={this.props.completedByOpponent} number={this.props.number} addPoints={this.props.addPoints}/>
			</div>
		)
	}
});

/*
	PointsCounter component: displayed when Row completed (and other player not) to count the points
	<PointsCounter />
 */

var PointsCounter = React.createClass({

	propTypes: {
		status: React.PropTypes.string.isRequired,
		amount: React.PropTypes.number.isRequired,
		completedByOpponent: React.PropTypes.bool.isRequired,
		number: React.PropTypes.number.isRequired,
		addPoints: React.PropTypes.func.isRequired
	},

	render: function() {
		var pointsClass = 'points-container row-item ' + this.props.status;
		var points = <span className="points-amount">{this.props.amount}</span>
		var button = '';
		if (!this.props.completedByOpponent) {
			button = <button type="button" onClick={this.props.addPoints.bind(null, this.props.number)}><span className="glyphicon glyphicon-plus"></span></button>
		}

		return (
			<span className={pointsClass}>
				{button}
				{points}
			</span>
		)
	}
});

/*
	PlayerTurner component: component to update turns
	<PlayerTurner />
 */

var PlayerTurner = React.createClass({

	propTypes: {
		changeTurn: React.PropTypes.func.isRequired
	},

	render: function() {
		return (
			<div className="turner-container">
				<div className="turner" onClick={this.props.changeTurn}>
					<img src="/build/css/images/icon-24-one-finger-tap.png"/>
				</div>
			</div>
		)
	}
});

/*
	Standings component: small container for standings in the game
	<Standings />
 */

var Standings = React.createClass({

	propTypes: {
		wonPlayer1: React.PropTypes.number.isRequired,
		wonPlayer2: React.PropTypes.number.isRequired
	},

	render: function() {
		return (
			<div className="standings">
				<h2>{this.props.wonPlayer1} - {this.props.wonPlayer2}</h2>
			</div>
		)
	}
});

/*
	Finished component: small overlay popup when game is finished
	<Finished />
 */

var Finished = React.createClass({

	propTypes: {
		player1: React.PropTypes.object.isRequired,
		player2: React.PropTypes.object.isRequired,
		resetGame: React.PropTypes.func.isRequired
	},

	render: function() {
		var points1 = h.calculateScore(this.props.player1.score);
		var points2 = h.calculateScore(this.props.player2.score);

		return (
			<div className="overlay-container">
				<div className="popup">
					<h2>Game shot</h2>
					<div className="popup-standings">
						<div className="row">
							<div className="col-md-4">{this.props.player1.name}</div>
							<div className="col-md-4">{points1}</div>
							<div className="col-md-4">{this.props.player1.roundsWon}</div>
						</div>
						<div className="row">
							<div className="col-md-4">{this.props.player2.name}</div>
							<div className="col-md-4">{points2}</div>
							<div className="col-md-4">{this.props.player2.roundsWon}</div>
						</div>
					</div>

					<div className="popup-buttons">
						<button type="button" className="btn btn-warning" onClick={this.props.resetGame}><span className="glyphicon glyphicon-play-circle"></span> Play again</button>
						<button type="button" className="btn btn-default"><span className="glyphicon glyphicon-menu-hamburger"></span> Menu</button>
					</div>
				</div>
			</div>
		)
	}
});

ReactDOM.render(<Tactics/>, document.querySelector('#main'));
